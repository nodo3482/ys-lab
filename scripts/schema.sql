CREATE SCHEMA `ys-lab` DEFAULT CHARACTER SET utf8mb4 ;

CREATE TABLE `ys-lab`.`user` (
    `user_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'user.id',
    `name` VARCHAR(50) NOT NULL COMMENT '이름',
    `sex` CHAR(1) NOT NULL COMMENT '성별 (M: 남자, F: 여자)',
    `age` INT(3) NOT NULL DEFAULT 0 COMMENT '나이',
    `birth` DATE NOT NULL COMMENT '생년월일',
    `phone` VARCHAR(255) NULL COMMENT '전화번호',
    `email` VARCHAR(255) NULL COMMENT '이메일',
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일',
    `update_date` DATETIME NULL COMMENT '수정일',
    PRIMARY KEY (`user_id`))
COMMENT = '회원';

CREATE TABLE `ys-lab`.`user_login_history` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'user_login_history.id',
    `user_id` BIGINT(20) NOT NULL COMMENT 'user.id',
    `is_success` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '로그인 성공여부(1: 성공, 0: 실패)',
    `login_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '로그인일시',
    PRIMARY KEY (`id`))
COMMENT = '회원 로그인내역';

CREATE TABLE `ys-lab`.`orders` (
    `order_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'orders.id',
    `order_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '주문일시',
    `user_id` BIGINT(20) NOT NULL COMMENT 'User.ID',
    `store_id` BIGINT(20) NOT NULL COMMENT 'Store.ID',
    `address` VARCHAR(255) NULL COMMENT '주소',
    `address_detail` VARCHAR(255) NULL COMMENT '나머지주소',
    `order_phone` VARCHAR(255) NULL COMMENT '주문자연락처',
    `memo` VARCHAR(255) NULL COMMENT '요청사항',
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
    PRIMARY KEY (`order_id`))
COMMENT = '주문';

CREATE TABLE `ys-lab`.`orders_detail` (
    `order_detail_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'orders_detail.ID',
    `order_id` BIGINT(20) NOT NULL COMMENT 'orders.id',
    `product_id` BIGINT(20) NOT NULL COMMENT 'product.id',
    `origin_price` INT(11) NOT NULL COMMENT '정상가격',
    `price` INT(11) NOT NULL COMMENT '구매가격',
    `quantity` SMALLINT(3) NOT NULL DEFAULT 1 COMMENT '구매수량',
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시',
    PRIMARY KEY (`order_detail_id`))
COMMENT = '주문상세';

CREATE TABLE `ys-lab`.`store` (
    `store_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'store.id',
    `name` VARCHAR(100) NULL COMMENT '매장명',
    `address` VARCHAR(255) NULL COMMENT '매장주소',
    `owner_name` VARCHAR(45) NULL COMMENT '매장 대표자명',
    `owner_phone` VARCHAR(255) NULL COMMENT '매장 전화번호',
    `registration_number` VARCHAR(10) NULL COMMENT '사업자등록번호',
    `open_date` DATE NULL DEFAULT CURRENT_TIMESTAMP COMMENT '오픈일',
    `status` SMALLINT(3) NOT NULL DEFAULT 1 COMMENT '1: 오픈준비중, 2: 영업중, 3: 영업중단, 4: 영업종료',
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
    `update_date` DATETIME NULL COMMENT '수정일시',
    `is_use` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '사용여부 (1: 사용, 0: 미사용)',
    PRIMARY KEY (`store_id`))
COMMENT = '매장';

CREATE TABLE `ys-lab`.`product` (
    `product_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'product.id',
    `store_id` BIGINT(20) NULL COMMENT 'store.id',
    `name` VARCHAR(100) NOT NULL COMMENT '상품명',
    `name_sub` VARCHAR(100) NULL COMMENT '상품명 (부제)',
    `origin_price` INT(11) NOT NULL COMMENT '정상가격',
    `price` INT(11) NOT NULL COMMENT '판매가격',
    `description` MEDIUMTEXT NULL COMMENT '상품설명',
    `is_sale` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '판매여부',
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
    `update_date` DATETIME NULL COMMENT '수정일시',
    PRIMARY KEY (`product_id`))
COMMENT = '상품';

CREATE TABLE `ys-lab`.`access_log` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'access_log.ID',
    `method` VARCHAR(45) NOT NULL COMMENT '호출 Method',
    `url` VARCHAR(500) NOT NULL COMMENT '접근URL',
    `header` MEDIUMTEXT NULL COMMENT 'Header정보',
    `query_string` MEDIUMTEXT NULL COMMENT 'QueryString',
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
    PRIMARY KEY (`id`))
COMMENT = 'API 접근로그';

CREATE TABLE `ys-lab`.`shedlock` (
    `name` VARCHAR(64) NOT NULL COMMENT '스케줄잠금 name',
    `lock_until` TIMESTAMP(3) NOT NULL COMMENT '잠금기간',
    `locked_at` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '잠금일시',
    `locked_by` VARCHAR(255) NOT NULL COMMENT '잠금신청자',
    PRIMARY KEY (`name`))
COMMENT = '스케줄잠금';