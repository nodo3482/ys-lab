package com.ys.test.lab.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Component
public class ApiCall {
    private final RestTemplate restTemplate;

    @Autowired
    public ApiCall(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
    }
    /*
        사용법
        GET, POST, PUT, DELETE - headerData : Header정보
        GET - data : queryParam으로 변환 (ex. map.put("user", "ys-lab") -> ?user=ys-lab)
        POST, PUT - data (Map<String, Object>) : Request Body값을 담아서 보냄
        GET, POST, PUT, DELETE - uriVariables (Map<String, String>) : pathVariable을 담아서 보냄(사용시, notnull 조건)
        (ex. /test/{value1}, map.put("value1", "23") -> /test/23)

        - Response Data는 String으로 받는다
        - pathVariable만 사용하고 싶을 경우, data 변수는 null로 넘겨 줄 것.
     */

    // -----------------get-----------------
    public ResponseEntity<String> get(String url, Map<String, String> headerData) {
        return restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> get(String url, Map<String, String> headerData, Map<String, String> data) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .encode()
                .queryParams(this.getParams(data));

        return restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> get(
            String url,
            Map<String, String> headerData,
            Map<String, String> data,
            Map<String, String> uriVariables
    ) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .encode()
                .queryParams(this.getParams(data));

        return restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class,
                uriVariables
        );
    }

    // -----------------post-----------------
    public ResponseEntity<String> post(String url, Map<String, String> headerData) {
        return restTemplate.exchange(
                url,
                HttpMethod.POST,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> post(String url, Map<String, String> headerData, Map<String, Object> data) {
        return restTemplate.exchange(
                url,
                HttpMethod.POST,
                new HttpEntity<>(data, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> post(
            String url,
            Map<String, String> headerData,
            Map<String, Object> data,
            Map<String, String> uriVariables
    ) {
        return restTemplate.exchange(
                url,
                HttpMethod.POST,
                new HttpEntity<>(data, this.setHeaders(headerData)),
                String.class,
                uriVariables
        );
    }

    // -----------------put-----------------
    public ResponseEntity<String> put(String url, Map<String, String> headerData) {
        return restTemplate.exchange(
                url,
                HttpMethod.PUT,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> put(String url, Map<String, String> headerData, Map<String, Object> data) {
        return restTemplate.exchange(
                url,
                HttpMethod.PUT,
                new HttpEntity<>(data, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> put(
            String url,
            Map<String, String> headerData,
            Map<String, Object> data,
            Map<String, String> uriVariables
    ) {
        return restTemplate.exchange(
                url,
                HttpMethod.PUT,
                new HttpEntity<>(data, this.setHeaders(headerData)),
                String.class,
                uriVariables
        );
    }

    // -----------------delete-----------------
    public ResponseEntity<String> delete(String url, Map<String, String> headerData) {
        return restTemplate.exchange(
                url,
                HttpMethod.DELETE,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class
        );
    }

    public ResponseEntity<String> delete(String url, Map<String, String> headerData, Map<String, String> uriVariables) {
        return restTemplate.exchange(
                url,
                HttpMethod.DELETE,
                new HttpEntity<>(null, this.setHeaders(headerData)),
                String.class,
                uriVariables
        );
    }

    // -----------------common method-----------------
    private MultiValueMap<String, String> getParams(Map<String, String> data) {
        if (ObjectUtils.isEmpty(data)) { // null check
            data = new HashMap<>();
        }
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        for (String key: data.keySet()) {
            params.add(key, data.get(key));
        }

        return params;
    }

    private HttpHeaders setHeaders(Map<String, String> headerData) {
        if (ObjectUtils.isEmpty(headerData)) {
            return null;
        }

        HttpHeaders headers = new HttpHeaders();
        for (String key : headerData.keySet()) {
            headers.add(key, headerData.get(key));
        }

        return headers;
    }
}
