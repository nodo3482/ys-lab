package com.ys.test.lab.common.enums;

import lombok.Getter;

@Getter
public enum CryptCode {
    ALGORITHM("PBEWithMD5AndDES"),
    PASSWORDCODE("ys-lab");

    private final String code;

    CryptCode(String code) {
        this.code = code;
    }
}
