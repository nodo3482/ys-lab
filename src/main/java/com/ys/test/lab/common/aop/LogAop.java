package com.ys.test.lab.common.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ys.test.lab.api.entity.AccessLogEntity;
import com.ys.test.lab.api.repository.AccessLogRepository;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
public class LogAop {
    private final AccessLogRepository accessLogRepository;

    @Autowired
    public LogAop(AccessLogRepository accessLogRepository) {
        this.accessLogRepository = accessLogRepository;
    }

    @Before("within(com.ys.test.lab.api.controller.*)")
    public void insertLog() throws JsonProcessingException {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        // headerInfo
        Enumeration<String> headers = request.getHeaderNames();
        Map<String, String> headerData = new HashMap<>();
        while(headers.hasMoreElements()) {
            String headerName = headers.nextElement();
            headerData.put(headerName, request.getHeader(headerName));
        }

        String headerInfo = "";
        if (headerData.size() > 0) {
            ObjectMapper mapper = new ObjectMapper();
            headerInfo = mapper.writeValueAsString(headerData);
        }

        AccessLogEntity accessLog = new AccessLogEntity();
        accessLog.setMethod(request.getMethod());
        accessLog.setUrl(request.getRequestURL().toString());
        accessLog.setHeader(headerInfo);
        accessLog.setQueryString(request.getQueryString());
        accessLogRepository.setAccessLog(accessLog);
    }
}
