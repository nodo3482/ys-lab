package com.ys.test.lab.common;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class File {
    public List<String> getFileReader(String fileUrl) {
        try(
            FileReader fileReader = new FileReader(fileUrl);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
                ) {
            return bufferedReader.lines().collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return new ArrayList<>();
    }
}
