package com.ys.test.lab.common.enums;

import lombok.Getter;

@Getter
public enum ResponseCode {
    SUCCESS(200, "정상 처리되었습니다."),
    FAIL(401, "응답에 실패했습니다."),
    EXCEPTION(500, "서버 통신중 오류가 발생했습니다. 잠시후 다시 시도해주세요.");

    private final int code;
    private final String message;

    ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
