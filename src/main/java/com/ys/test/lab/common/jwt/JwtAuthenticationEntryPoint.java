package com.ys.test.lab.common.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.common.enums.ResponseCode;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        Response data = new Response(ResponseCode.FAIL.getCode(), "잘못된 토큰정보입니다.", new HashMap<>());
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(data));
    }
}
