package com.ys.test.lab.common.exceptions;

import com.ys.test.lab.api.dto.common.Response;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Objects;

@RestControllerAdvice
@EnableWebMvc
@Log4j2
public class CommonException {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> commonException(Exception e) {
        log.error("exception error: ", e);
        return Response.exception(e.getMessage());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<Response> pageNotFoundException(NoHandlerFoundException e) {
        return Response.fail("페이지를 찾을 수 없습니다.");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Response> validException(MethodArgumentNotValidException e) {
        return Response.fail(Objects.requireNonNull(e.getFieldError()).getDefaultMessage());
    }
}
