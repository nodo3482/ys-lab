package com.ys.test.lab.common;

import com.ys.test.lab.common.enums.CryptCode;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

// 문자열 암복호화
public class Crypt {
    public String encryptString(String arg) {
        StandardPBEStringEncryptor pbeEnc = new StandardPBEStringEncryptor();
        pbeEnc.setAlgorithm(CryptCode.ALGORITHM.getCode());
        pbeEnc.setPassword(CryptCode.PASSWORDCODE.getCode());
        return pbeEnc.encrypt(arg);
    }

    public String decryptString(String arg) {
        StandardPBEStringEncryptor pbeEnc = new StandardPBEStringEncryptor();
        pbeEnc.setAlgorithm(CryptCode.ALGORITHM.getCode());
        pbeEnc.setPassword(CryptCode.PASSWORDCODE.getCode());
        return pbeEnc.decrypt(arg);
    }
}
