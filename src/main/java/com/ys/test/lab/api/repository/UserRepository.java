package com.ys.test.lab.api.repository;

import com.ys.test.lab.api.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

// JpaRepository를 상속받으면, 자동으로 Bean에 등록된다.
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Page<UserEntity> findAll(Pageable pageable);
    UserEntity findByUserId(Long userId);
    int countByUserId(Long userId);
}
