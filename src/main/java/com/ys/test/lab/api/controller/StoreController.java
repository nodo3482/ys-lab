package com.ys.test.lab.api.controller;

import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.dto.request.StoreRequestDto;
import com.ys.test.lab.api.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class StoreController {
    private final StoreService storeService;

    @Autowired
    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    // 매장 목록
    @GetMapping(value = "/stores")
    public ResponseEntity<Response> getStores(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "20") int size) {
        return Response.ok(storeService.getStores(page, size));
    }

    // 매장 상세
    @GetMapping(value = "/stores/{store_id:^[0-9]+$}")
    public ResponseEntity<Response> getStoreDetail(@PathVariable("store_id") Long storeId) {
        return Response.ok(storeService.getStoreDetail(storeId));
    }

    // 매장 등록
    @PostMapping(value = "/stores")
    public ResponseEntity<Response> addStore(@Valid @RequestBody StoreRequestDto request) {
        return storeService.addStore(request);
    }

    // 매장 수정
    @PutMapping(value = "/stores/{store_id:^[0-9]+$}")
    public ResponseEntity<Response> updateStore(
            @PathVariable("store_id") Long storeId,
            @Valid @RequestBody StoreRequestDto request) {
        return storeService.updateStore(storeId, request);
    }

    // 매장 삭제
    @DeleteMapping(value = "/stores/{store_id:^[0-9]+$}")
    public ResponseEntity<Response> deleteStore(@PathVariable("store_id") Long storeId) {
        return storeService.deleteStore(storeId);
    }
}
