package com.ys.test.lab.api.controller;

import com.ys.test.lab.api.dto.request.UserRequestDto;
import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    // user 목록
    @GetMapping(value = "/users")
    public ResponseEntity<Response> getUsers(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "20") int size) {
        Map<String, Object> whereClause = new HashMap<String, Object>();
        return Response.ok(userService.getUsers(page, size, whereClause));
    }

    // user 상세
    @GetMapping(value = "/user/{user_id:^[0-9]+$}")
    public ResponseEntity<Response> getUserDetail(@PathVariable("user_id") Long userId) {
        return Response.ok(userService.getUserDetail(userId));
    }

    // user 등록
    @PostMapping(value = "/user")
    public ResponseEntity<Response> addUser(@Valid @RequestBody UserRequestDto request) {
        return userService.addUser(request);
    }

    // user 수정
    @PutMapping(value = "/user/{user_id:^[0-9]+$}")
    public ResponseEntity<Response> modifyUser(
            @PathVariable("user_id") Long userId,
            @Valid @RequestBody UserRequestDto request) {
        return userService.updateUser(userId, request);
    }

    // user 삭제
    @DeleteMapping(value = "/user/{user_id:^[0-9]+$}")
    public ResponseEntity<Response> deleteUser(@PathVariable("user_id") Long userId) {
        return userService.deleteUser(userId);
    }
}
