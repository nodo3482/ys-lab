package com.ys.test.lab.api.dto.common;

import com.ys.test.lab.common.enums.ResponseCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.Map;

@Getter
@Setter
public class Response {
    private Integer code;
    private String message;
    private Map<String, Object> data;

    public Response(Integer code, String message, Map<String, Object> data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    // 정상 응답
    public static ResponseEntity<Response> ok() {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                new Response(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), null)
        );
    }

    public static ResponseEntity<Response> ok(Map<String, Object> data) {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                new Response(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), data)
        );
    }

    // 실패 응답
    public static ResponseEntity<Response> fail() {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                new Response(ResponseCode.FAIL.getCode(), ResponseCode.FAIL.getMessage(), null)
        );
    }

    public static ResponseEntity<Response> fail(String message) {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                new Response(ResponseCode.FAIL.getCode(), message, null)
        );
    }

    // 서버(로직) 오류 응답
    public static ResponseEntity<Response> exception() {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                new Response(ResponseCode.EXCEPTION.getCode(), ResponseCode.EXCEPTION.getMessage(), null)
        );
    }

    public static ResponseEntity<Response> exception(String message) {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                new Response(ResponseCode.EXCEPTION.getCode(), message, null)
        );
    }
}
