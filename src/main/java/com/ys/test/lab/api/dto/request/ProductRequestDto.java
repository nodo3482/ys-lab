package com.ys.test.lab.api.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductRequestDto {
    @NotEmpty(message = "name을 입력해주세요.")
    private String name;

    private String nameSub;

    @NotNull(message = "originPrice를 입력해주세요.")
    private Integer originPrice;

    @NotNull(message = "price를 입력해주세요.")
    private Integer price;

    private String description;

    @Min(value = 0, message = "is_use범위를 올바로 입력해주세요.")
    @Max(value = 1, message = "is_use범위를 올바로 입력해주세요.")
    private Byte isSale;
}
