package com.ys.test.lab.api.dto.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class StoreRequestDto {
    private String name;

    private String address;

    private String ownerName;

    private String ownerPhone;

    @Length(max = 10)
    private String registrationNumber;

    @Pattern(
            regexp = "^(19[0-9][0-9]|20\\d{2})-(0[0-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$",
            message = "open_date는 YYYY-MM-DD 형식으로 입력이 가능합니다."
    )
    @NotEmpty(message = "open_date를 입력해주세요.")
    private String openDate;

    @NotNull(message = "status값을 입력해주세요.")
    private Short status;

    @Min(value = 0, message = "is_use범위를 올바로 입력해주세요.")
    @Max(value = 1, message = "is_use범위를 올바로 입력해주세요.")
    private Byte isUse;
}
