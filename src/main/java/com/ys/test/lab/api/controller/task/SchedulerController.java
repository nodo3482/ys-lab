package com.ys.test.lab.api.controller.task;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

//@Controller
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S", defaultLockAtLeastFor = "PT30S") // (기본 세팅)30초동안 Lock
public class SchedulerController {
    @Scheduled(cron = "*/20 * * * * *") // 매 20초마다 Task 수행
    @SchedulerLock(name = "schedulerStart1", lockAtMostFor = "PT19S", lockAtLeastFor = "PT19S")
   public void schedulerStart1() {
        System.out.println("Hello! scheduler 1");
    }

//    @Scheduled(fixedDelay = 2000) // 스케줄러가 끝나는 시간 기준으로 2000ms (2초) 간격으로 Task 수행
    public void schedulerStart2() {
        System.out.println("Hello! scheduler 2");
    }

//    @Scheduled(fixedRate = 3000) // 스케줄러가 시작하는 시간 기준으로 3000ms (3초) 간격으로 Task 수행
    public void schedulerStart3() {
        System.out.println("Hello! scheduler 3");
    }
}
