package com.ys.test.lab.api.controller;

import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.dto.request.AuthRequestDto;
import com.ys.test.lab.api.service.UserService;
import com.ys.test.lab.common.jwt.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AuthController {
    private final UserService userService;
    private final TokenProvider tokenProvider;

    @Autowired
    public AuthController(UserService userService, TokenProvider tokenProvider) {
        this.userService = userService;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping(value = "/token")
    public ResponseEntity<Response> getToken(@Valid @RequestBody AuthRequestDto request) {
        // 이걸로는 AuthenticationToken 생성이 안되는 것 같다. 왜안될까??
        // => SpringSecurity에서는 Authentication생성 할 때 받는 UsernamePasswordAuthenticationToken의
        // 두번째 parameter(credentials)가 password 암호가 진행되는듯. 참고로 SpringSecurityConfig의 passwordEncoder()를 @Bean으로 등록한다.

        /*
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        */
        if (!userService.isExistUser(Long.parseLong(request.getUserId()))) {
            return Response.fail("회원정보가 없습니다.");
        }

        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_USER");
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                String.valueOf(request.getUserId()), "", Collections.singleton(grantedAuthority)); // 권한은 고정

        SecurityContextHolder.getContext().setAuthentication(authentication);

        // 인증정보 기반으로 JWT 토큰 생성
        String token = tokenProvider.createToken(authentication);
        Map<String, Object> data = new HashMap<>();
        data.put("token", "Bearer " + token);
        return Response.ok(data);
    }
}
