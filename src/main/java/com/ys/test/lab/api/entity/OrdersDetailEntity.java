package com.ys.test.lab.api.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Entity
@Table(name = "orders_detail")
public class OrdersDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderDetailId;

    @ManyToOne
    @JoinColumn(name = "orderId")
    private OrdersEntity ordersEntity;

    @ManyToOne
    @JoinColumn(name = "productId")
    private ProductEntity productEntity;

    private Integer originPrice;

    private Integer price;

    private Short quantity;

    private LocalDateTime createDate;
}
