package com.ys.test.lab.api.repository;

import com.ys.test.lab.api.entity.AccessLogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

// USE JdbcTemplate
@Repository
public class AccessLogRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AccessLogRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int setAccessLog(AccessLogEntity accessLog) {
        String sql = "" +
                "INSERT INTO" +
                "    `ys-lab`.`access_log`" +
                "    (`method`, `url`, `header`, `query_string`, `create_date`)" +
                "VALUES" +
                "    (?, ?, ?, ?, ?)";
        Object[] params = {
                accessLog.getMethod(),
                accessLog.getUrl(),
                accessLog.getHeader(),
                accessLog.getQueryString(),
                LocalDateTime.now()
        };
        return jdbcTemplate.update(sql, params);
    }
}
