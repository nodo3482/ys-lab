package com.ys.test.lab.api.service;

import com.ys.test.lab.api.dto.request.UserRequestDto;
import com.ys.test.lab.api.dto.response.UserResponseDto;
import com.ys.test.lab.api.dto.common.Paging;
import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.entity.UserEntity;
import com.ys.test.lab.api.repository.UserRepository;
import com.ys.test.lab.config.ModelMapperBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 스프링 데이터 JPA Repository 사용 예제
 */
@Service
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapperBean modelMapperBean;

    @Autowired
    public UserService(
            UserRepository userRepository,
            ModelMapperBean modelMapperBean
    ) {
        this.userRepository = userRepository;
        this.modelMapperBean = modelMapperBean;
    }

    // user list
    public Map<String, Object> getUsers(int page, int size, Map<String, Object> whereClause) {
        Page<UserEntity> data = userRepository.findAll(PageRequest.of(Integer.max(0, page-1), size));
        Pageable pageable = data.getPageable();
        List<UserEntity> users = data.getContent();

        ModelMapper modelMapper = modelMapperBean.modelMapper();
        List<UserResponseDto> response = users
                .stream()
                .map(x -> modelMapper.map(x, UserResponseDto.class))
                .collect(Collectors.toList());

        Map<String, Object> returnData = new HashMap<>();
        returnData.put("users", response);
        returnData.put("paging", Paging.setPaging(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                data.getTotalPages())
        );

        return returnData;
    }

    // userDetail
    public Map<String, Object> getUserDetail(Long userId) {
        UserEntity user = userRepository.findByUserId(userId);
        Map<String, Object> returnData = new HashMap<>();
        if (Optional.ofNullable(user).isEmpty()) {
            returnData.put("user", null);
        } else {
            ModelMapper modelMapper = modelMapperBean.modelMapper();
            UserResponseDto response = modelMapper.map(user, UserResponseDto.class);

            returnData.put("user", response);
        }

        return returnData;
    }

    // user add
    public ResponseEntity<Response> addUser(UserRequestDto params) {
        ModelMapper modelMapper = modelMapperBean.modelMapper();
        UserEntity userEntity = modelMapper.map(params, UserEntity.class);
        userEntity.setBirth(LocalDate.parse(params.getBirth(), DateTimeFormatter.ISO_LOCAL_DATE));
        userEntity.setCreateDate(LocalDateTime.now());
        userEntity.setUpdateDate(LocalDateTime.now());

        userRepository.save(userEntity);

        return Response.ok();
    }

    // user update
    public ResponseEntity<Response> updateUser(Long userId, UserRequestDto params) {
        UserEntity userEntity = userRepository.findByUserId(userId);
        if (userEntity == null) {
            return Response.fail("데이터가 없습니다.");
        }

        ModelMapper modelMapper = modelMapperBean.modelMapper();
        modelMapper.map(params, userEntity);

        userEntity.setBirth(LocalDate.parse(params.getBirth(), DateTimeFormatter.ISO_LOCAL_DATE));
        userEntity.setUpdateDate(LocalDateTime.now());

        userRepository.save(userEntity);
        return Response.ok();
    }

    // user delete
    public ResponseEntity<Response> deleteUser(Long userId) {
        userRepository.deleteById(userId);
        return Response.ok();
    }

    // user 존재여부
    public boolean isExistUser(Long userId) {
        return userRepository.countByUserId(userId) > 0;
    }
}
