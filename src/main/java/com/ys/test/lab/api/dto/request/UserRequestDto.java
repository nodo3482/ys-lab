package com.ys.test.lab.api.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
public class UserRequestDto {
    @NotEmpty(message = "name값을 입력해주세요.")
    private String name;

    @NotBlank(message = "sex를 입력해주세요.")
    @Pattern(regexp = "^[MF]$", message = "sex는 (M, F)입력만 가능합니다.")
    private String sex;

    @NotNull(message = "age를 입력해주세요.")
    private Integer age;

    @NotNull(message = "birth를 입력해주세요.")
    @Pattern(
            regexp = "^(19[0-9][0-9]|20\\d{2})-(0[0-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$",
            message = "birth는 YYYY-MM-DD 형식으로 입력이 가능합니다."
    )
    private String birth;

    private String phone;

    private String email;
}
