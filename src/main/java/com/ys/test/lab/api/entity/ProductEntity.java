package com.ys.test.lab.api.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString(exclude = "storeEntity")
@Entity
@Table(name = "product")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;

    @ManyToOne
    @JoinColumn(name = "storeId")
    private StoreEntity storeEntity;

    private String name;

    private String nameSub;

    private Integer originPrice;

    private Integer price;

    private String description;

    private Integer isSale;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;
}
