package com.ys.test.lab.api.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@Table(name = "store")
public class StoreEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long storeId;

    private String name;

    private String address;

    private String ownerName;

    private String ownerPhone;

    private String registrationNumber;

    private LocalDate openDate;

    private Short status;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

    private Byte isUse;

    @OneToMany(mappedBy = "storeEntity", fetch = FetchType.LAZY)
    private List<OrdersEntity> ordersEntityList = new ArrayList<>();

    @OneToMany(mappedBy = "storeEntity", fetch = FetchType.LAZY)
    private List<ProductEntity> productEntityList = new ArrayList<>();
}
