package com.ys.test.lab.api.service;

import com.ys.test.lab.api.dto.request.StoreRequestDto;
import com.ys.test.lab.api.dto.response.StoreResponseDto;
import com.ys.test.lab.api.dto.common.Paging;
import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.entity.StoreEntity;
import com.ys.test.lab.api.repository.StoreRepository;
import com.ys.test.lab.config.ModelMapperBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * JPA EntityManager 사용 예제
 */
@Service
public class StoreService {
    private final StoreRepository storeRepository;
    private final ModelMapperBean modelMapperBean;

    @Autowired
    public StoreService(
            StoreRepository storeRepository,
            ModelMapperBean modelMapperBean
    ) {
        this.storeRepository = storeRepository;
        this.modelMapperBean = modelMapperBean;
    }

    public Map<String, Object> getStores(int page, int size) {
        // @TODO JPQL, PersistenceContext에서 Paging기능 관련하여 사용할 수 있는 방법이 있을까?
        List<StoreEntity> stores = storeRepository.getStores(page, size);
        int totalCount = storeRepository.getStoreCount();

        ModelMapper modelMapper = modelMapperBean.modelMapper();
        AtomicInteger index = new AtomicInteger();
        List<StoreResponseDto> response = stores
                .stream()
                .map(x -> modelMapper.map(x, StoreResponseDto.class))
//                .peek(x -> x.setProductCount(stores.get(index.getAndIncrement()).getProductEntityList().size()))
                .collect(Collectors.toList());

        // @TODO 매장당 SELECT Query가 발생하여 성능이슈가 발생. (매장이 10개라면, 쿼리는 11번 수행됨)
        Map<String, Object> returnData = new HashMap<>();
        returnData.put("stores", response);
        returnData.put("paging", Paging.setPaging(page, size, Paging.getTotalPage(totalCount, size)));

        return returnData;
    }

    // storeDetail
    public Map<String, Object> getStoreDetail(Long storeId) {
        StoreEntity store = storeRepository.getStoreDetail(storeId);
        Map<String, Object> returnData = new HashMap<>();

        if (Optional.ofNullable(store).isEmpty()) {
            returnData.put("store", null);
        } else {
            ModelMapper modelMapper = modelMapperBean.modelMapper();
            StoreResponseDto response = modelMapper.map(store, StoreResponseDto.class);
            response.setProductCount(store.getProductEntityList().size()); // 연관관계 - 지연로딩
            returnData.put("store", response);
        }

        return returnData;
    }

    // store add
    public ResponseEntity<Response> addStore(StoreRequestDto params) {
        ModelMapper modelMapper = modelMapperBean.modelMapper();
        StoreEntity storeEntity = modelMapper.map(params, StoreEntity.class);
        storeEntity.setOpenDate(LocalDate.parse(params.getOpenDate(), DateTimeFormatter.ISO_LOCAL_DATE));
        storeEntity.setCreateDate(LocalDateTime.now());
        storeEntity.setUpdateDate(LocalDateTime.now());

        storeRepository.setStore(storeEntity);
        return Response.ok();
    }

    // store update
    public ResponseEntity<Response> updateStore(Long storeId, StoreRequestDto params) {
        StoreEntity storeEntity = storeRepository.getStoreDetail(storeId);
        if (storeEntity == null) {
            return Response.fail("데이터가 없습니다.");
        }

        ModelMapper modelMapper = modelMapperBean.modelMapper();
        modelMapper.map(params, storeEntity);

        storeEntity.setOpenDate(LocalDate.parse(params.getOpenDate(), DateTimeFormatter.ISO_LOCAL_DATE));
        storeEntity.setUpdateDate(LocalDateTime.now());

        storeRepository.setStore(storeEntity);
        return Response.ok();
    }

    // store delete
    public ResponseEntity<Response> deleteStore(Long storeId) {
        StoreEntity storeEntity = storeRepository.getStoreDetail(storeId);
        if (storeEntity == null) {
            return Response.fail("데이터가 없습니다.");
        }

        storeRepository.deleteStore(storeEntity);
        return Response.ok();
    }
}
