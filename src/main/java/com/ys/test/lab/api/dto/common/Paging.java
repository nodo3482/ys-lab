package com.ys.test.lab.api.dto.common;

import java.util.HashMap;
import java.util.Map;

public class Paging {
    public static Map<String, Object> setPaging(int page, int size, int totalPage) {
        Map<String, Object> paging = new HashMap<>();
        paging.put("page", Integer.max(page, 1));
        paging.put("size", size);
        paging.put("totalPage", totalPage);
        return paging;
    }

    // 총 페이지 수
    public static int getTotalPage(int totalCount, int size) {
        return (totalCount < 1) ? 1 : Math.floorDiv(totalCount, size) +
                ((Math.floorMod(totalCount, size) < 1) ? 0 : 1);
    }
}
