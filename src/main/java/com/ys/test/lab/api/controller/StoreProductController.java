package com.ys.test.lab.api.controller;

import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.dto.request.ProductRequestDto;
import com.ys.test.lab.api.service.StoreProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class StoreProductController {
    private final StoreProductService storeProductService;

    @Autowired
    public StoreProductController(StoreProductService storeProductService) {
        this.storeProductService = storeProductService;
    }

    // 매장에 해당하는 상품
    @GetMapping(value = "/stores/{store_id:^[0-9]+$}/products")
    public ResponseEntity<Response> getStoreProducts(
            @PathVariable("store_id") Long storeId,
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "size", defaultValue = "20") int size) {
        return Response.ok(storeProductService.getStoreProducts(storeId, page, size));
    }

    // 매장에 해당하는 상품 상세
    @GetMapping(value = "/stores/{store_id:^[0-9]+$}/products/{product_id:^[0-9]+$}")
    public ResponseEntity<Response> getStoreProductDetail(
            @PathVariable("store_id") Long storeId,
            @PathVariable("product_id") Long productId) {
        return Response.ok(storeProductService.getStoreProductDetail(storeId, productId));
    }

    // 매장 상품 등록
    @PostMapping(value = "/stores/{store_id:^[0-9]+$}/products")
    public ResponseEntity<Response> addStoreProduct(
            @PathVariable("store_id") Long storeId,
            @Valid @RequestBody ProductRequestDto request) {
        return storeProductService.addStoreProduct(storeId, request);
    }

    // 매장 상품 수정
    @PutMapping(value = "/stores/{store_id:^[0-9]+$}/products/{product_id:^[0-9]+$}")
    public ResponseEntity<Response> modifyProduct(
            @PathVariable("store_id") Long storeId,
            @PathVariable("product_id") Long productId,
            @Valid @RequestBody ProductRequestDto request) {
        return storeProductService.updateStoreProduct(productId, request);
    }

    // 매장 상품 삭제
    @DeleteMapping(value = "/stores/{store_id:^[0-9]+$}/products/{product_id:^[0-9]+$}")
    public ResponseEntity<Response> deleteProduct(
            @PathVariable("store_id") Long storeId,
            @PathVariable("product_id") Long productId) {
        return storeProductService.deleteStoreProduct(productId);
    }
}
