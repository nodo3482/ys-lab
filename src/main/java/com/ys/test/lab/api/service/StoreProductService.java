package com.ys.test.lab.api.service;

import com.ys.test.lab.api.dto.common.Paging;
import com.ys.test.lab.api.dto.common.Response;
import com.ys.test.lab.api.dto.request.ProductRequestDto;
import com.ys.test.lab.api.dto.response.ProductResponseDto;
import com.ys.test.lab.api.entity.ProductEntity;
import com.ys.test.lab.api.repository.StoreProductRepository;
import com.ys.test.lab.config.ModelMapperBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StoreProductService {
    private final StoreProductRepository storeProductRepository;
    private final ModelMapperBean modelMapperBean;

    @Autowired
    public StoreProductService(
            StoreProductRepository storeProductRepository,
            ModelMapperBean modelMapperBean
    ) {
        this.storeProductRepository = storeProductRepository;
        this.modelMapperBean = modelMapperBean;
    }

    // storeProduct
    public Map<String, Object> getStoreProducts(Long storeId, int page, int size) {
        List<ProductEntity> products = storeProductRepository.getProducts(storeId, page, size);
        int totalCount = storeProductRepository.getProductCount(storeId);

        ModelMapper modelMapper = modelMapperBean.modelMapper();
        List<ProductResponseDto> response = products
                .stream()
                .map(x -> modelMapper.map(x, ProductResponseDto.class))
                .collect(Collectors.toList());

        Map<String, Object> returnData = new HashMap<>();
        returnData.put("products", response);
        returnData.put("paging", Paging.setPaging(page, size, Paging.getTotalPage(totalCount, size)));
        return returnData;
    }

    // storeProductDetail
    public Map<String, Object> getStoreProductDetail(Long storeId, Long productId) {
        ProductEntity product = storeProductRepository.getProductDetail(storeId, productId);
        ModelMapper modelMapper = modelMapperBean.modelMapper();
        ProductResponseDto response = modelMapper.map(product, ProductResponseDto.class);

        Map<String, Object> returnData = new HashMap<>();
        returnData.put("product", response);
        return returnData;
    }

    // storeProduct add
    public ResponseEntity<Response> addStoreProduct(Long storeId, ProductRequestDto productRequestDto) {
        return (storeProductRepository.setProduct(storeId, productRequestDto)) ? Response.ok() : Response.fail();
    }

    // storeProduct update
    public ResponseEntity<Response> updateStoreProduct(
            Long productId,
            ProductRequestDto productRequestDto) {
        return (storeProductRepository.updateProduct(productId, productRequestDto)) ? Response.ok() : Response.fail();
    }

    // storeProduct delete
    public ResponseEntity<Response> deleteStoreProduct(Long productId) {
        return (storeProductRepository.deleteProduct(productId)) ? Response.ok() : Response.fail();
    }
}
