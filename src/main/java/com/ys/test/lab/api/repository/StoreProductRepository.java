package com.ys.test.lab.api.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ys.test.lab.api.dto.request.ProductRequestDto;
import com.ys.test.lab.api.entity.ProductEntity;
import com.ys.test.lab.api.entity.QProductEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class StoreProductRepository {
    @PersistenceContext
    private final EntityManager entityManager;

    public StoreProductRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // 매장의 상품 조회
    public List<ProductEntity> getProducts(Long storeId, int page, int size) {
        int startPosition = (page - 1) * size;

        JPAQueryFactory query = new JPAQueryFactory(this.entityManager);
        QProductEntity qProductEntity = new QProductEntity("product");
        return query.selectFrom(qProductEntity)
                .where(qProductEntity.storeEntity.storeId.eq(storeId))
                .orderBy(qProductEntity.productId.desc())
                .offset(startPosition)
                .limit(size)
                .fetch();
    }

    // 매장의 상품 총 등록수
    public Integer getProductCount(Long storeId) {
        JPAQueryFactory query = new JPAQueryFactory(this.entityManager);
        QProductEntity qProductEntity = new QProductEntity("productCount");
        return query.select(qProductEntity.productId.count())
                .from(qProductEntity)
                .where(qProductEntity.storeEntity.storeId.eq(storeId))
                .fetchOne().intValue();
    }

    // 매장의 상품 상세조회
    public ProductEntity getProductDetail(Long storeId, Long productId) {
        JPAQueryFactory query = new JPAQueryFactory(this.entityManager);
        QProductEntity qProductEntity = new QProductEntity("productDetail");
        return query.selectFrom(qProductEntity)
                .where(qProductEntity.storeEntity.storeId.eq(storeId)
                .and(qProductEntity.productId.eq(productId)))
                .fetchOne();
    }

    // 매장 상품 등록
    // Use NativeQuery
    @Transactional
    public boolean setProduct(Long storeId, ProductRequestDto productRequestDto) {
        EntityManager entityManager = this.entityManager;

        // store_id만 등록해야하는데, Entity를 Mapping할 수 없다.
        String sql = "" +
                "INSERT INTO" +
                "   `ys-lab`.`product`" +
                "   (store_id, `name`, name_sub, origin_price, price, description, is_sale)" +
                "VALUES" +
                "   (?, ?, ?, ?, ?, ?, ?)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, storeId);
        query.setParameter(2, productRequestDto.getName());
        query.setParameter(3, productRequestDto.getNameSub());
        query.setParameter(4, productRequestDto.getOriginPrice());
        query.setParameter(5, productRequestDto.getPrice());
        query.setParameter(6, productRequestDto.getDescription());
        query.setParameter(7, productRequestDto.getIsSale());

        return (query.executeUpdate() == 1);
    }

    @Transactional
    public boolean updateProduct(Long productId, ProductRequestDto productRequestDto) {
        JPAQueryFactory query = new JPAQueryFactory(this.entityManager);
        QProductEntity qProductEntity = new QProductEntity("updateProduct");
        long result = query.update(qProductEntity)
                .set(qProductEntity.name, productRequestDto.getName())
                .set(qProductEntity.nameSub, productRequestDto.getNameSub())
                .set(qProductEntity.originPrice, productRequestDto.getOriginPrice())
                .set(qProductEntity.price, productRequestDto.getPrice())
                .set(qProductEntity.description, productRequestDto.getDescription())
                .set(qProductEntity.isSale, productRequestDto.getIsSale().intValue())
                .set(qProductEntity.updateDate, LocalDateTime.now())
                .where(qProductEntity.productId.eq(productId))
                .execute();
        return (result == 1);
    }

    @Transactional
    public boolean deleteProduct(Long productId) {
        JPAQueryFactory query = new JPAQueryFactory(this.entityManager);
        QProductEntity qProductEntity = new QProductEntity("updateProduct");
        long result = query.delete(qProductEntity)
                .where(qProductEntity.productId.eq(productId))
                .execute();
        return (result == 1);
    }
}
