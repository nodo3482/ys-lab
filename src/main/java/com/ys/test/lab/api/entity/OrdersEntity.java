package com.ys.test.lab.api.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@Table(name = "orders")
public class OrdersEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    private LocalDateTime orderDate;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "storeId")
    private StoreEntity storeEntity;

    private String address;

    private String addressDetail;

    private String orderPhone;

    private String memo;

    private LocalDateTime createDate;

    @OneToMany(mappedBy = "ordersEntity", fetch = FetchType.LAZY)
    private List<OrdersDetailEntity> ordersDetailEntityList = new ArrayList<>();
}
