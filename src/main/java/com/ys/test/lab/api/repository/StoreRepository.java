package com.ys.test.lab.api.repository;

import com.ys.test.lab.api.entity.StoreEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class StoreRepository {
    @PersistenceContext
    private final EntityManager entityManager;

    public StoreRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // 목록 조회
    public List<StoreEntity> getStores(int page, int size) {
        int startPosition = (page - 1) * size;

        String sql = "SELECT s FROM StoreEntity s";
        return entityManager.createQuery(sql, StoreEntity.class)
                .setFirstResult(startPosition)
                .setMaxResults(size)
                .getResultList();
    }

    // 총 매장 목록수
    public int getStoreCount() {
        String sql = "SELECT COUNT(s) FROM StoreEntity s";
        Long count = entityManager.createQuery(sql, Long.class).getSingleResult();
        return count.intValue();
    }

    // 조회
    public StoreEntity getStoreDetail(Long storeId) {
        return entityManager.find(StoreEntity.class, storeId);
    }

    // 저장 & 수정
    @Transactional
    public void setStore(StoreEntity storeEntity) {
        entityManager.persist(storeEntity); // + flush()
    }

    @Transactional
    public void deleteStore(StoreEntity storeEntity) {
        entityManager.remove(storeEntity);
    }
}
