package com.ys.test.lab.api.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/*
    IntelliJ - 아래 Table name value의 Cannot resolve table 'user' 오류 해결방법
    Preferences... > Editor > Inspection > JPA > Unresolved database references in annotations 체크 해제
 */
@Getter
@Setter
@ToString
@Entity
@Table(name = "user")
public class UserEntity {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    private String name;

    private String sex;

    private Integer age;

    private LocalDate birth;

    private String phone;

    private String email;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

    @OneToMany(mappedBy = "userEntity", fetch = FetchType.LAZY)
    private List<UserLoginHistoryEntity> userLoginHistoryEntityList;

    @OneToMany(mappedBy = "storeEntity", fetch = FetchType.LAZY)
    private List<OrdersEntity> ordersEntityList;
}
