package com.ys.test.lab.api.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class AccessLogEntity {
    private Long id;

    private String method;

    private String url;

    private String header;

    private String queryString;

    private LocalDateTime createDate;
}
