package com.ys.test.lab;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BeanTests {
    @Autowired
    private DefaultListableBeanFactory beanFactory;

    // 프로젝트에 등록된 Bean의 목록을 확인할 수 있음
    @Disabled
    @Test
    void beanCheckTest() {
        for (String name : beanFactory.getBeanDefinitionNames()) {
            System.out.println("Add Beans => " + beanFactory.getBean(name).getClass().getName());
        }
    }
}
