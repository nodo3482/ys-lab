package com.ys.test.lab;

import com.ys.test.lab.common.File;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileReaderTests {
    File file;
    @BeforeEach
    public void load() {
        this.file = new File();
    }

    /**
     * 첫번째 정보 가져오기
     */
    @Test
    public void getMemberTestOne() {
        List<String> files = file.getFileReader("src/main/resources/contents/members.csv");
        String[] memberInfo = memberArr(files.get(0));
        assertEquals("Tom", memberInfo[0]);
        assertEquals("점프투자바", memberInfo[1]);
    }

    /**
     * Tom 회원의 보유도서수
     */
    @Test
    public void getMemberTestMany() {
        List<String> files = file.getFileReader("src/main/resources/contents/members.csv");
        long count = files.stream().filter(s -> memberArr(s)[0].equals("Tom")).count();
        assertEquals(3, count);
    }

    private String[] memberArr(String memberStr) {
        return memberStr.split(",");
    }
}
